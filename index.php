<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

require '/public/vendor/autoload.php';
require '/public/src/MyApp/Chat.php';

use MyApp\Chat;

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Chat()
        )
    )
    , 8080
);

$server->run();
