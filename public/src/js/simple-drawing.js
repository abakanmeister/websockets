let app = new Vue({
    el: '#app',
    // delimiters: ['${', '}'],
    data: {
        clickX: [],
        clickY: [],
        clickDrag: [],
        clickColor: [],
        clickSize: [],
        paint: false,
        canvas: null,
        context: null,
        curColor: null,
        curSize: null,
        colors: {
            purple: "#cb3594",
            green: "#659b41",
            yellow: "#ffcf33",
            brown: "#986928"
        },
        sizes: {
            small: 5,
            normal: 15,
            bold: 30,
            monster: 50
        },
        buttons: null,
        width: null,
        height: null,
        isConnected: false,
        socket: null
    },
    methods: {
        addClick(x, y, dragging)
        {
            this.clickX.push(x);
            this.clickY.push(y);
            this.clickDrag.push(dragging);
            this.clickColor.push(this.curColor);
            this.clickSize.push(this.curSize);
            this.socket.send(JSON.stringify({
                x: x,
                y: y,
                dragging: dragging,
                color: this.curColor,
                size: this.curSize
            }));
        },
        handleMouseDown(){
            let mouseX = event.pageX - this.canvas.offsetLeft,
                mouseY = event.pageY - this.canvas.offsetTop;
            this.paint = true;
            this.addClick(mouseX, mouseY);
            this.redraw();
        },
        handleMouseMove(){
            if(this.paint){
                let canvas = this.canvas;
                this.addClick(event.pageX - canvas.offsetLeft, event.pageY - canvas.offsetTop, true);
                this.redraw();
            }
        },
        redraw(color = null, size = null){
            let context = this.context,
                clickX = this.clickX,
                clickY = this.clickY,
                clickDrag = this.clickDrag;
            context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
            context.strokeStyle = "#df4b26";
            context.lineJoin = "round";
            context.lineWidth = 5;
            for(var i=0; i < clickX.length; i++) {
                context.beginPath();
                if(clickDrag[i] && i){
                    context.moveTo(clickX[i-1], clickY[i-1]);
                }else{
                    context.moveTo(clickX[i]-1, clickY[i]);
                }
                context.lineTo(clickX[i], clickY[i]);
                context.closePath();
                context.strokeStyle = color ? color : this.clickColor[i];
                context.lineWidth = size ? size : this.clickSize[i];
                context.stroke();
            }
        },
        changeColor(key){
            this.curColor = this.colors[key];
        },
        changeSize(key){
            this.curSize = this.sizes[key];
            [...document.getElementsByClassName('size-button')]
                .map(x => x.removeAttribute('style'));
            event.target.style.backgroundColor = 'lightblue';
        },
        clearCanvas(){
            let context = this.context;
            context.beginPath();
            context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        },
        resizeCanvas(height, width){
            let canvas = this.canvas;
            canvas.height = height;
            canvas.width= width;
        },
        createButtons(){
            this.buttons = Object.keys(this.colors);
        },
        initCanvas(){
            let canvas = document.getElementsByTagName('canvas')[0],
                context,
                height = Math.floor(window.outerHeight / 2),
                width = Math.floor(window.outerWidth / 1.2),
                colors = this.colors;
            canvas.width = width;
            canvas.height = height;
            canvas.id = 'canvas';
            canvas.style.border = '1px solid black';
            canvas.style.borderRadius = '5px';
            this.canvas = canvas;
            this.context = canvas.getContext('2d');
            this.context.lineWidth = 40;
            this.curColor = this.colors.purple;
            $(window).resize(x => {
                height = Math.floor(x.target.outerHeight / 2);
                width = Math.floor(x.target.outerWidth / 1.2);
                this.resizeCanvas(height, width)
            });
            this.height = height;
            this.width = width;
            Object.keys(colors).forEach(key => {
                if (document.getElementById(key)) {
                    document.getElementById(key).style.backgroundColor = colors[key];
                }
            });
        },
        initSocket(){
            let uri = 'wss://127.0.0.1:8080/';
            if (this.isConnected) {
                setOffline();
                return;
            }
            let conn = new WebSocket(uri);
            conn.onmessage = function(e) {
                let data = JSON.parse(e.data);
                this.clickX.push(data.x);
                this.clickX.push(data.y);
                this.clickDrag.push(data.dragging);
                this.redraw(data.color, data.size)
            }
            conn.onopen = function(e) {
                this.isConnected = true;
                console.log("Connected");
            };
            conn.onclose = function(e) {
                this.isConnected = false;
                console.log("Disconnected");
            };
            this.socket = conn;
        }
    },
    mounted(){
        this.initSocket();
        this.initCanvas();
        document.getElementsByClassName('size-button')[0].style.backgroundColor = 'lightblue';
    }
});






