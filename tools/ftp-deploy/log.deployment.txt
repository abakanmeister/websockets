Started at [2018/11/06 11:49]
Config file is /var/www/websockets/tools/ftp-deploy/deployment.ini

Deploying test-server
Note: connection is not encrypted
Live mode
Connecting to server
Loaded remote .htdeployment file
Scanning files in /var/www/websockets
Ignoring ./vendor                       
Ignoring ./tools/ftp-deploy             
Ignoring ./composer.lock                
Ignoring ./.htaccess                    
Ignoring ./.git                         
Ignoring ./.idea                        
Ignoring ./.gitignore                   
Creating remote file .htdeployment.running

Uploading:
(1 of 4) /public/web/websocket.html
(2 of 4) /public/bin/chat-server.php
(3 of 4) /public/src/js/simple-drawing.js
(4 of 4) /.htdeployment

Renaming:
(1 of 4) Renaming /public/web/websocket.html
(2 of 4) Renaming /public/bin/chat-server.php
(3 of 4) Renaming /public/src/js/simple-drawing.js
(4 of 4) Renaming /.htdeployment

Deleting remote file .htdeployment.running

Finished at [2018/11/06 11:49] (in 6 seconds)
